#Zoraida:zgonza16@gmail.com

@tag
Feature: Categor�a Input Forms
As a web user
I want to used the Input Forms category
to validate words and sum of whole numbers

 @tag1
Scenario: Simple Form Demo
Given that Emily wants to enter a word in the "Casa" text box.
When she writes the word test
Then she should see the same word "Casa" on the screen

 
@tag2
Scenario Outline: Simple Form Demo Two
 Given that Emily wants to enter two whole numbers
 When she enters the number <numeroa>, <numerob>
 Then she should see on the screen the <resultado>

      Examples: 
      | numeroa   | numerob   | resultado   |
      |    "5"    |    "4"    | "9"         |
      |    "7"    |    "3"    | "10"        |
   







