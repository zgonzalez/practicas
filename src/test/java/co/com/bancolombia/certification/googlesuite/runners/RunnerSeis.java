package co.com.bancolombia.certification.googlesuite.runners;

import org.junit.runner.RunWith;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src\\test\\resources\\features\\practicaseis.feature",//mapea la historia
        glue = "co.com.bancolombia.certification.googlesuite.stepdefinitions", //mapea los steps
        snippets =SnippetType.CAMELCASE) //estandar de java

public class RunnerSeis {

}
