package co.com.bancolombia.certification.googlesuite.stepdefinitions;
import org.openqa.selenium.WebDriver;
import co.com.bancolombia.certification.googlesuite.questionseis.QuestionsEmilySeis;
import co.com.bancolombia.certification.googlesuite.questionseis.QuestionEmilySeisTwo;
import co.com.bancolombia.certification.googlesuite.questionseis.SeleccionarTable;
import co.com.bancolombia.certification.googlesuite.questionseis.BuscarVacio;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.taskseis.DataTableSearchForm;
import co.com.bancolombia.certification.googlesuite.taskseis.DataTableSearchFormTwo;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumHomePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacesseis.SeleniumTableSearchPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Managed;
//dos import nuevos
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItems;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.Matcher.*;

public class PracticaSeisStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver herBrowser;
	
	private Actor Emily=Actor.named("Emily");
	private SeleniumHomePage seleniumHomePage;
	
	@Before
    public void setUP(){
		Emily.can(BrowseTheWeb.with(herBrowser));	
	}
	

	@Given("^that Emily wants select the category Table Search$")
	public void thatEmilyWantsSelectTheCategoryTableSearch() {
	    Emily.wasAbleTo(OpenTheBrowser.on(seleniumHomePage),
	    		GoTo.theApp(SeleniumAppsComponent.SELENIUM_EASY_APP));    		
	}


	@When("^she clicks on the search engine and typs  \"([^\"]*)\"$")
	public void sheClicksOnTheSearchEngineAndTyps(String arg1) {
		Emily.wasAbleTo(DataTableSearchForm.the(arg1)); 
		
	}

	@Then("^she should see the task typs  \"([^\"]*)\" Bug fixing on the screen$")
	public void sheShouldSeeTheTaskTypsBugFixingOnTheScreen(String arg1) {
		Emily.should(seeThat(QuestionsEmilySeis.displayed(),equalTo(arg1) ));
		 System.out.println(QuestionsEmilySeis.displayed().answeredBy(Emily));
	}
	
	//Escenario dos

	@When("^she clicks search and writes \"([^\"]*)\"$")
	public void sheClicksSearchAndWrites(String arg1) {
		Emily.wasAbleTo(DataTableSearchFormTwo.the(arg1)); 
	}
	
	@Then("^she should see that the filter did not produce results and the message \"([^\"]*)\"$")
	public void sheShouldSeeThatTheFilterDidNotProduceResultsAndTheMessage(String arg1) {
		Emily.should(seeThat(QuestionEmilySeisTwo.displayed(),equalTo(arg1) ));
		 System.out.println(QuestionEmilySeisTwo.displayed().answeredBy(Emily));
	}
	
}
