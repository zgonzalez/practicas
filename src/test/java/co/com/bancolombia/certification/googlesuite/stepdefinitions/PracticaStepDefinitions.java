package co.com.bancolombia.certification.googlesuite.stepdefinitions;

import org.openqa.selenium.WebDriver;
import co.com.bancolombia.certification.googlesuite.questions.QuestionsEmily;
import co.com.bancolombia.certification.googlesuite.questions.QuestionsEmilySum;
import co.com.bancolombia.certification.googlesuite.questions.Prueba;
import co.com.bancolombia.certification.googlesuite.questions.Suma;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.tasks.SimpleForm;
import co.com.bancolombia.certification.googlesuite.tasks.SimpleFormTwo;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumHomePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePageTwo;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Managed;
//dos import nuevos
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItems;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.Matcher.*;


public class PracticaStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver herBrowser;
	
	private Actor Emily=Actor.named("Emily");
	private SeleniumHomePage seleniumHomePage;
	
	@Before
    public void setUP(){
		Emily.can(BrowseTheWeb.with(herBrowser));	
	}
	
	@Given("^that Emily wants to enter a word in the \"([^\"]*)\" text box\\.$")
	public void thatEmilianaWantsToEnterAWordInTheTextBox(String arg1) { //se le pasa el valor "Casa"a la variable arg1(esta variable puede ser cualquier nombre) 
	    Emily.wasAbleTo(OpenTheBrowser.on(seleniumHomePage),
	    		GoTo.theApp(SeleniumAppsComponent.SELENIUM_EASY_APP),
	    		GoTo.theApp(SeleniumAppsComponent.SIMPLE_FORM_DEMO),
	    		Enter.theValue(arg1).into(SeleniumSimplePage.WRITE_MESSAGE),//se le pasa tambien la variable arg1
				Click.on(SeleniumSimplePage.SHOW_MESSAGE_BUTTON));
	}
	
	@When("^she writes the word test$")
	public void sheWritesTheWordTest() {
		   Emily.wasAbleTo( SimpleForm.the());  
	}
	
	@Then("^she should see the same word \"([^\"]*)\" on the screen$")
	public void sheShouldSeeTheSameWordTestOnTheScreen(String palabra) {
		Emily.should(seeThat(QuestionsEmily.displayed(),equalTo(palabra) ));
		 System.out.println(QuestionsEmily.displayed().answeredBy(Emily));
	
	}
	
	//Escenario dos

	@Given("^that Emily wants to enter two whole numbers$")
	public void thatEmilyWantsToEnterTwoWholeNumbers(){
		Emily.wasAbleTo(OpenTheBrowser.on(seleniumHomePage),
	    		GoTo.theApp(SeleniumAppsComponent.SELENIUM_EASY_APP),
	    		GoTo.theApp(SeleniumAppsComponent.SIMPLE_FORM_DEMO));
	}

	
	@When("^she enters the number \"([^\"]*)\", \"([^\"]*)\"$")
	public void sheEntersTheNumber(String arg1, String arg2) {
		Emily.wasAbleTo(SimpleFormTwo.the(arg1,arg2)); 
	}
	
	@Then("^she should see on the screen the \"([^\"]*)\"$")
	public void sheShouldSeeOnTheScreenThe(String resultado) {
		Emily.should(seeThat(QuestionsEmilySum.displayed(),equalTo(resultado) ));
		 System.out.println(QuestionsEmilySum.displayed().answeredBy(Emily));
	 
	 }


}
