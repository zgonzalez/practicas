package co.com.bancolombia.certification.googlesuite.stepdefinitions;
import co.com.bancolombia.certification.googlesuite.questionscuatro.QuestionsEmilyCuatro;
import co.com.bancolombia.certification.googlesuite.questionsiete.QuestionsEmilySiete;
import co.com.bancolombia.certification.googlesuite.questionsiete.SeleccionAlerts;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.taskseis.DataTableSearchForm;
import co.com.bancolombia.certification.googlesuite.tasksiete.AlertsModalsForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumHomePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacesseis.SeleniumTableSearchPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Managed;
//dos import nuevos
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItems;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.Matcher.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

public class PracticaSieteStepDefinitions {

	@Managed(driver="chrome")
	private WebDriver herBrowser;
	
	private Actor Emily=Actor.named("Emily");
	private SeleniumHomePage seleniumHomePage;
	
	@Before
    public void setUP(){
		Emily.can(BrowseTheWeb.with(herBrowser));	
	}
	
	@Given("^that Emily wants select the category Javascript Alerts$")
	public void thatEmilyWantsSelectTheCategoryJavascriptAlerts() {
		Emily.wasAbleTo(OpenTheBrowser.on(seleniumHomePage),
	    		GoTo.theApp(SeleniumAppsComponent.SELENIUM_EASY_APP));
		
//		System.out.println(alert.getText());
	}


	@When("^she clicks on the for Prompt Box \"([^\"]*)\"$")
	public void sheClicksOnTheForPromptBox(String arg1) {
		Emily.wasAbleTo(AlertsModalsForm.the(arg1)); 
	}

	@When("^Enter the word \"([^\"]*)\"\\.$")
	public void enterTheWord(String arg1) {
	   
	}

	@Then("^she should see the text \"([^\"]*)\"  on the screen$")
	public void sheShouldSeeTheTextOnTheScreen(String arg1) {
		Emily.should(seeThat(QuestionsEmilySiete.displayed(),equalTo(arg1) ));
		 System.out.println(QuestionsEmilySiete.displayed().answeredBy(Emily));
	}

	
	
}
