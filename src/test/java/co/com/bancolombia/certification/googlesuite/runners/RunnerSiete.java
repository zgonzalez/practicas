package co.com.bancolombia.certification.googlesuite.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src\\test\\resources\\features\\practicasiete.feature",//mapea la historia
        glue = "co.com.bancolombia.certification.googlesuite.stepdefinitions", //mapea los steps
        snippets =SnippetType.CAMELCASE) //estandar de java
public class RunnerSiete {

}
