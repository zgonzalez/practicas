package co.com.bancolombia.certification.googlesuite.stepdefinitions;
import org.openqa.selenium.WebDriver;
import co.com.bancolombia.certification.googlesuite.questionstres.QuestionsEmilyTres;
import co.com.bancolombia.certification.googlesuite.questionstres.SeleccionFemale;
import co.com.bancolombia.certification.googlesuite.tasktres.RadioButtonForm;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumHomePage;
import co.com.bancolombia.certification.googlesuite.userinterfacestres.SeleniumRadioPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Managed;
//dos import nuevos
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.hasItems;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.Matcher.*;


public class PracticaTresStepDefinitions {
	@Managed(driver="chrome")
	private WebDriver herBrowser;
	
	private Actor Emily=Actor.named("Emily");
	private SeleniumHomePage seleniumHomePage;
	
	@Before
    public void setUP(){
		Emily.can(BrowseTheWeb.with(herBrowser));	
	}
	
	@Given("^that Emily wants select the category Radio Button$")
	public void thatEmilyWantsSelectTheCategoryRadioButton() {
		Emily.wasAbleTo(OpenTheBrowser.on(seleniumHomePage),
				GoTo.theApp(SeleniumAppsComponent.SELENIUM_EASY_APP));  
	}


	@When("^she clicks on the option female$")
	public void sheClicksOnTheOptionFemale() {
		Emily.wasAbleTo( RadioButtonForm.the());
	}

	@Then("^she should see the text \"([^\"]*)\" on the screen$")
	public void sheShouldSeeTheTextOnTheScreen(String arg1) {
		Emily.should(seeThat(QuestionsEmilyTres.displayed(),equalTo(arg1) ));
		 System.out.println(QuestionsEmilyTres.displayed().answeredBy(Emily));
	}



}
