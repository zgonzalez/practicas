package co.com.bancolombia.certification.googlesuite.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumSimplePageTwo {
	
	public static final Target ENTER_A =Target.the("Por favor ingresar el numero entero A").located(By.id("sum1"));
    public static final Target ENTER_B =Target.the("Por favor ingresar el numero entero B").located(By.id("sum2"));
    public static final Target GET_TOTAL =Target.the("Clic en el boton muestra el total de la suma").located(By.xpath("//*[@id='gettotal']/button"));
    public static final Target SUMA =Target.the("Muestra el total de la suma").located(By.id("displayvalue"));
     
}
