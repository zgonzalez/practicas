package co.com.bancolombia.certification.googlesuite.questionscuatro;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Text;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.hamcrest.Matchers.hasItems;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;//lo importe borrar
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.questionscuatro.SeleccionDia;
import co.com.bancolombia.certification.googlesuite.questionstres.QuestionsEmilyTres;
import co.com.bancolombia.certification.googlesuite.questionstres.SeleccionFemale;
import co.com.bancolombia.certification.googlesuite.questionscuatro.QuestionsEmilyCuatro;
import co.com.bancolombia.certification.googlesuite.taskcuatro.DropdownListForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import groovyjarjarantlr.collections.List;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;


public class QuestionsEmilyCuatro implements Question <String> {

	//Metodo
			@Override
			public String answeredBy(Actor actor)  {
				return SeleccionDia.ITEMS.resolveFor(actor).getText();
				 }
				
			//retorna
			  public static Question <String> displayed() {              
			        return new QuestionsEmilyCuatro();

			    }	
			  
}
