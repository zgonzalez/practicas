package co.com.bancolombia.certification.googlesuite.userinterfacessiete;

import org.openqa.selenium.Alert;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumAlertsyModalsPage {
	public static final Target SELECCION_ALERTS_MODALS =Target.the("clic en alerts modals").located(By.xpath("//*[@id='treemenu']/li/ul/li[5]/a"));
	public static final Target CLIC_JAVASCRIP_ALERTS =Target.the("Se da clic en javascrip alerts").located(By.xpath("//*[@id='treemenu']/li/ul/li[5]/ul/li[5]/a"));
	public static final Target CLIC_BUTTON_PROMPT_BOX =Target.the("Se da clic en el boton Click for Prompt Box").located(By.xpath("//*[@id='easycont']/div/div[2]/div[3]/div[2]/button"));
	public static final Target CLIC_ACEPTAR =Target.the("Se da clic en el boton Aceptar").located(By.xpath(""));
	//public static final Target VALIDAR_PALABRA =Target.the("Se valida la palabra 'Refuerzo Automatización'").located(By.id("prompt-demo"));

	
}
