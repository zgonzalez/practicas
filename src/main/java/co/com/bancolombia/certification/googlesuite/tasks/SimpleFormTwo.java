package co.com.bancolombia.certification.googlesuite.tasks;

import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePageTwo;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.InstrumentedTask;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class SimpleFormTwo implements Task {
	public String numeroa; //atributos de la clase
	public String numerob;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
			
				Enter.theValue(numeroa).into(SeleniumSimplePageTwo.ENTER_A),
				Enter.theValue(numerob).into(SeleniumSimplePageTwo.ENTER_B),
				Click.on(SeleniumSimplePageTwo.GET_TOTAL)
				);	
	}

	public static SimpleFormTwo the(String numeroa, String numerob) { 
	
		return Instrumented.instanceOf(SimpleFormTwo.class).withProperties(numeroa,numerob);//(instanceOf retorna la misma clase) (permite pasarle la propiedad o el parametro .withProperties)
	}
	
	public SimpleFormTwo(String numeroa, String numerob){
     this.numeroa=numeroa;
	 this.numerob=numerob;
	}

}

