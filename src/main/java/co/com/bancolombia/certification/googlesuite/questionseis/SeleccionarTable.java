package co.com.bancolombia.certification.googlesuite.questionseis;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleccionarTable {
	public static Target ITEMS= Target.the("Verificar opci�n siete en la tabla de busqueda")
			.located(By.xpath("//*[@id='task-table']/tbody/tr[7]/td[1]")); //Se verifica la opci�n correcta
}
