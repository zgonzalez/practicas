package co.com.bancolombia.certification.googlesuite.userinterfacescuatro;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumDropdownListPage {
	public static final Target SELECCION_DROPDOWN_LIST =Target.the("clic en Dropdown List").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/ul/li[4]/a"));
	public static final Target CLIC_LISTA =Target.the("Se da clic en la lista").located(By.id("select-demo"));
	public static final Target CLIC_DIA =Target.the("Se selecciona el dia de la semana").located(By.xpath("//*[@value='Saturday']"));
	public static final Target VALIDAR_DIA =Target.the("Se verifica que se haya seleccionado la opcion del dia correcto").located(By.xpath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[2]"));
	
}
