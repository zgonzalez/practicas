package co.com.bancolombia.certification.googlesuite.userinterfacescinco;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;


public class SeleniumJQuerySelectPage {
	public static final Target SELECCION_JQUERY =Target.the("clic en JQuery Select dropdown").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/ul/li[7]/a"));
	public static final Target CLIC_LISTA_PAISES =Target.the("Se da clic en la lista de paises").located(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div[2]/span/span[1]/span/span[2]"));
	public static final Target CLIC_PAIS =Target.the("Se selecciona el pais").located(By.xpath("//*[@id='country']/option[6]"));
 }
