package co.com.bancolombia.certification.googlesuite.tasktres;

import co.com.bancolombia.certification.googlesuite.tasktres.RadioButtonForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacestres.SeleniumRadioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class RadioButtonForm implements Task {
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				Click.on(SeleniumRadioPage.SELECCION_RADIO_B),
		        Click.on(SeleniumRadioPage.CLIC_FEMALE),
		        Click.on(SeleniumRadioPage.CLIC_CHEKED));
		//Espera de java
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
  }
	
	public static RadioButtonForm the() {	
		return Tasks.instrumented(RadioButtonForm.class);
	}
	
}
