package co.com.bancolombia.certification.googlesuite.tasksiete;



import org.openqa.selenium.Alert;

import co.com.bancolombia.certification.googlesuite.tasksiete.AlertsModalsForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacessiete.SeleniumAlertsyModalsPage;


import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Switch;

import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class AlertsModalsForm implements Task{
	
public String palabra; //atributos de la clase

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAlertsyModalsPage.SELECCION_ALERTS_MODALS),
				Click.on(SeleniumAlertsyModalsPage.CLIC_JAVASCRIP_ALERTS),
		        Click.on(SeleniumAlertsyModalsPage.CLIC_BUTTON_PROMPT_BOX)
//		        Switch.toAlert()
		        );
				
				Alert alert = BrowseTheWeb.as(actor).getAlert();
				alert.sendKeys(this.palabra);
				alert.accept();
				//System.out.println(alert.getText());
//		        SendKeys("hola")
//		        SendKeys("hola"),
//		        Enter.(palabra).into(Switch.toAlert().getClass())
//		        Enter.theValue("cosa").into(target)
//		        =driver.switchTo().alert();
//				driver.switchTo().alert().sendKeys("Helllo");
//				alert.accept();
//				System.out.println(alert.getText());
//		        Enter.theValue(palabra).into(SeleniumAlertsyModalsPage.VALIDAR_PALABRA)
//				);	
//		actor.attemptsTo(Switch.toAlert());
//		Alert alert=driver.switchTo().alert()
		
		//Espera de java
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
	    	
  }
  
	public static AlertsModalsForm the( String palabra) {	
		return Instrumented.instanceOf(AlertsModalsForm.class).withProperties(palabra);
	}
	
	public AlertsModalsForm(String palabra){
		 this.palabra=palabra;
	}


}
