package co.com.bancolombia.certification.googlesuite.taskseis;

import net.serenitybdd.screenplay.Task;
import co.com.bancolombia.certification.googlesuite.tasks.SimpleFormTwo;
import co.com.bancolombia.certification.googlesuite.taskseis.DataTableSearchForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacesdos.SeleniumCheckboxPage;
import co.com.bancolombia.certification.googlesuite.userinterfacesseis.SeleniumTableSearchPage;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class DataTableSearchFormTwo implements Task{
public String numerocho; //atributos de la clase
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				Click.on(SeleniumTableSearchPage.SELECCION_TABLE),
		        Click.on(SeleniumTableSearchPage.CLIC_DATA_TABLE_SEARCH),
		        Click.on(SeleniumTableSearchPage.CLIC_SEARCH),
		        Enter.theValue(numerocho).into(SeleniumTableSearchPage.CLIC_SEARCH)
				);	
		/*	
		//Espera de java
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
	    	*/
  }
	
	public static DataTableSearchFormTwo the( String numerocho) {	
		return Instrumented.instanceOf(DataTableSearchFormTwo.class).withProperties(numerocho);
	}
	
	public DataTableSearchFormTwo(String numerocho){
		 this.numerocho=numerocho;
	}

}
