package co.com.bancolombia.certification.googlesuite.userinterfacesseis;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumTableSearchPage {
	public static final Target SELECCION_TABLE =Target.the("clic en table").located(By.xpath("//*[@id='treemenu']/li/ul/li[3]/a"));
	public static final Target CLIC_DATA_TABLE_SEARCH =Target.the("Se da clic en Table Data Search  ").located(By.xpath("//*[@id='treemenu']/li/ul/li[3]/ul/li[2]/a"));
	public static final Target CLIC_SEARCH =Target.the("Se da clic en la busqueda").located(By.id("task-table-filter")); 
	
}
