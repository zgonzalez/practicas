package co.com.bancolombia.certification.googlesuite.taskcuatro;

import co.com.bancolombia.certification.googlesuite.taskcuatro.DropdownListForm;
import co.com.bancolombia.certification.googlesuite.tasktres.RadioButtonForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacescuatro.SeleniumDropdownListPage;
import co.com.bancolombia.certification.googlesuite.userinterfacestres.SeleniumRadioPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class DropdownListForm implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				Click.on(SeleniumDropdownListPage.SELECCION_DROPDOWN_LIST),
		        Click.on(SeleniumDropdownListPage.CLIC_LISTA),
		        Click.on(SeleniumDropdownListPage.CLIC_DIA));
		
		//Espera de java
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
  }
	
	public static DropdownListForm the() {	
		return Tasks.instrumented(DropdownListForm.class);
	}
	
}
