package co.com.bancolombia.certification.googlesuite.userinterfacestres;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumRadioPage {
	public static final Target SELECCION_RADIO_B =Target.the("clic en radio button").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/ul/li[3]/a"));
	public static final Target CLIC_FEMALE =Target.the("Se da clic en la opci�n Female ").located(By.xpath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/label[2]/input"));
	public static final Target CLIC_CHEKED =Target.the("Se da clic en el boton Get cheked value").located(By.id("buttoncheck"));
	public static final Target VALIDAR_RADIO_B =Target.the("Se verifica que haya seleccionado la opcion Female").located(By.xpath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[3]"));

}
