package co.com.bancolombia.certification.googlesuite.taskcinco;

import co.com.bancolombia.certification.googlesuite.taskcinco.JQuerySelectForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfacescinco.SeleniumJQuerySelectPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class JQuerySelectForm implements Task{
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				Click.on(SeleniumJQuerySelectPage.SELECCION_JQUERY),
		        Click.on(SeleniumJQuerySelectPage.CLIC_LISTA_PAISES),
		        Click.on(SeleniumJQuerySelectPage.CLIC_PAIS));
		
		//Espera de java
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
  }
	
	public static JQuerySelectForm the() {	
		return Tasks.instrumented(JQuerySelectForm.class);
	}

}
