package co.com.bancolombia.certification.googlesuite.questionsiete;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Text;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.hamcrest.Matchers.hasItems;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.questionsiete.SeleccionAlerts;
import co.com.bancolombia.certification.googlesuite.questionsiete.QuestionsEmilySiete;
import co.com.bancolombia.certification.googlesuite.questionstres.QuestionsEmilyTres;
import co.com.bancolombia.certification.googlesuite.questionstres.SeleccionFemale;
import co.com.bancolombia.certification.googlesuite.taskdos.CheckboxForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import groovyjarjarantlr.collections.List;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;

public class QuestionsEmilySiete implements Question <String> {
	//Metodo
			@Override
			public String answeredBy(Actor actor)  {
				return SeleccionAlerts.ITEMS.resolveFor(actor).getText();
				 }
				
			//viendo tutorial retorna
			  public static Question <String> displayed() {              
			        return new QuestionsEmilySiete();

			    }	

}
