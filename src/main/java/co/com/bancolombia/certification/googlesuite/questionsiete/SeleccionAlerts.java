package co.com.bancolombia.certification.googlesuite.questionsiete;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleccionAlerts {
	public static Target ITEMS= Target.the("Verificar Mensaje “Refuerzo Automatización” del Alert")
			.located(By.id("prompt-demo")); 
}
