package co.com.bancolombia.certification.googlesuite.questionstres;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleccionFemale {

	public static Target ITEMS= Target.the("Verificar Radio Button female")
			.located(By.xpath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[3]")); //Se verifica la opci�n correcta
}
