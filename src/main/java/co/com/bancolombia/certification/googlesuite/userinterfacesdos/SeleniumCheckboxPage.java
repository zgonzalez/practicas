package co.com.bancolombia.certification.googlesuite.userinterfacesdos;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumCheckboxPage {
   
	public static final Target SELECCION_CHECKBOX =Target.the("clic en checbox demo").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/ul/li[2]/a"));
	public static final Target CLIC_CHECK =Target.the("Se da clic en on this check box ").located(By.id("isAgeSelected"));
	public static final Target VALIDAR_CHECK =Target.the("Se verifica que haya seleccionado la opcion Success - Check box is checked").located(By.id("txtAge"));
}
