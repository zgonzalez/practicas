package co.com.bancolombia.certification.googlesuite.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleniumSimplePage {

    public static final Target WRITE_MESSAGE =Target.the("Por favor ingresar el mensaje").located(By.id("user-message"));
    public static final Target SHOW_MESSAGE_BUTTON =Target.the("Clic en el botton ver mensaje").located(By.xpath("//*[@id='get-input']/button"));
    public static final Target SHOW_MESSAGE =Target.the("ver mensaje introducido en pantalla").located(By.id("display"));
     
}
    