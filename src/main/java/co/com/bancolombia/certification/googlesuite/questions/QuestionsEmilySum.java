package co.com.bancolombia.certification.googlesuite.questions;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Text;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.hamcrest.Matchers.hasItems;
import co.com.bancolombia.certification.googlesuite.tasks.GoTo;//lo importe borrar
import co.com.bancolombia.certification.googlesuite.tasks.OpenTheBrowser;
import co.com.bancolombia.certification.googlesuite.tasks.SimpleForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import groovyjarjarantlr.collections.List;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;

public class QuestionsEmilySum implements Question <String> {

	//Metodo
		@Override
		public String answeredBy(Actor actor)  {
			return Suma.ITEMS.resolveFor(actor).getText();
			 }
			
		//viendo tutorial retorna
		  public static Question <String> displayed() {              
		        return new QuestionsEmilySum();

		    }		
	
}
