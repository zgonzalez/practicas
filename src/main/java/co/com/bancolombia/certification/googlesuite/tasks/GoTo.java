package co.com.bancolombia.certification.googlesuite.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.targets.Target;

//Aqui pasamos la pagina WEB
public class GoTo implements Task{ // se debe implementar simpre tareas
	
	private Target seleniumEasyApp;// se crea el atributo de la clase
	//private Target simpleFormDemo;// se crea el atributo de la clase
	
	// se crea el constructor de la clase
	public GoTo (Target seleniumEasyApp){
		this.seleniumEasyApp=seleniumEasyApp;
		//this.simpleFormDemo=simpleFormDemo;
	
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
		Click.on(seleniumEasyApp));   // se pasa como parametro Selenium Easy
		//actor.attemptsTo(Click.on(SeleniumAppsComponent.SIMPLE_FORM_DEMO),
		//Click.on(simpleFormDemo)); // se pasa como parametro SIMPLE_FORM_DEMO)
		//aqui me falta el Clic.on mas adelante reviso
	}

	public static GoTo theApp(Target seleniumEasyApp) {
		return Tasks.instrumented(GoTo.class,seleniumEasyApp);
	}
	

	
}

