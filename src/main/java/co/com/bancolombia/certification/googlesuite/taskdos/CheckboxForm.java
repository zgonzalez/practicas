package co.com.bancolombia.certification.googlesuite.taskdos;

import co.com.bancolombia.certification.googlesuite.tasktres.RadioButtonForm;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfacesdos.SeleniumCheckboxPage;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class CheckboxForm implements Task{

	public String texto; //atributos de la clase
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				Click.on(SeleniumCheckboxPage.SELECCION_CHECKBOX),
		        Click.on(SeleniumCheckboxPage.CLIC_CHECK));
		//Espera de java esta espera la puse para poder verificar la prueba por que se ejecutaba muy rapido
	    try {
	    	Thread.sleep(10000); 
	    	} catch(InterruptedException ex) {
	    	Thread.currentThread().interrupt();
	    	}
  }
	
	public static CheckboxForm the() {	
		return Tasks.instrumented(CheckboxForm.class);
	}


	/*//metodo
	public CheckboxForm(String texto){
	     this.texto=texto;
		}*/
	
}

