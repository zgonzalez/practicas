package co.com.bancolombia.certification.googlesuite.userinterfaces;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;


public class SeleniumAppsComponent {
	public static final Target SELENIUM_EASY_APP = Target.the("Selenium Apps imput form").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/a"));                                                                                                                 
    public static final Target SIMPLE_FORM_DEMO = Target.the("Selenium Form Demo").located(By.xpath("//*[@id='treemenu']/li/ul/li[1]/ul/li[1]/a"));
                                                   
}

