package co.com.bancolombia.certification.googlesuite.tasks;

import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumAppsComponent;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePage;
import co.com.bancolombia.certification.googlesuite.userinterfaces.SeleniumSimplePageTwo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class SimpleForm implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				//Click.on(SeleniumAppsComponent.SELENIUM_EASY_APP),
				//Click.on(SeleniumAppsComponent.SIMPLE_FORM_DEMO),
				//Enter.theValue("Prueba").into(SeleniumSimplePage.WRITE_MESSAGE),
				//Click.on(SeleniumSimplePage.SHOW_MESSAGE_BUTTON)
				);			
	}

	public static SimpleForm the() {	
		return Tasks.instrumented(SimpleForm.class);
	}

}

