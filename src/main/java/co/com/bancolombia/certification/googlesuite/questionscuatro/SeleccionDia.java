package co.com.bancolombia.certification.googlesuite.questionscuatro;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class SeleccionDia {

	public static Target ITEMS= Target.the("Verificar que el dia seleccionado corresponda al enviado")
			.located(By.xpath("//*[@id='easycont']/div/div[2]/div[1]/div[2]/p[2]")); 
}

