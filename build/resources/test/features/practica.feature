#Zoraida:zgonza16@gmail.com

Feature: Categor�a Input Forms
As a web user
I want to used the Input Forms category
to validate words and sum of whole numbers
Background: 

Scenario: Simple Form Demo
Given that Emily wants to enter a word in the "Enter message" text box.
When she writes the word test
Then she should see the same word test on the screen

Scenario Outline:
Given that Emily wants to enter two whole numbers
When she enters the number "<ENTER_A>", "<ENTER_B>"
Then she should see on the screen the correct sum of the two whole numbers

Examples: 
      |ENTER_A|ENTER_B|
      | 5     | 7     |
      | 5     | 5     |






