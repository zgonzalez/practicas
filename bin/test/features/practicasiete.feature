
Feature: Categoría Javascript Alerts 
As a web user
I want to used the Javascript Alerts  category
to Enter the word Reinforcement Automation and validate that this text was entered

Scenario: Javascript Alerts 
Given that Emily wants select the category Javascript Alerts
When she clicks on the for Prompt Box "Refuerzo Automatización"
And Enter the word "Refuerzo Automatización".
Then she should see the text "You have entered 'Refuerzo Automatización' !"  on the screen

  